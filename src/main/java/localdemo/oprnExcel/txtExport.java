package localdemo.oprnExcel;

import localdemo.MyException.ObjException;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class txtExport {

    private static String filenameTemp;


    /**
     * 创建文件
     *
     * @throws IOException
     */

    public static boolean creatTxtFile(String name) {
        boolean flag = false;
        filenameTemp = name;
        File filename = new File(filenameTemp);
        if (!filename.exists()) {
            try {
                filename.createNewFile();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            flag = true;
        }
        System.out.println(filenameTemp);
        return flag;
    }

    public static void writeExcelFile (String path, String fileName, int tableNum, SXSSFWorkbook workbook) throws IOException {
        // 文件夹名
        if (!path.endsWith("\\")) {
            path = path + "\\";
        }

        // 验证路径是否合法
        File dir = new File(path);
        if (!dir.exists() || !dir.isDirectory()) {
            throw new IllegalArgumentException("路径不合法或不存在: " + path);
        }

        String fieldName = path + fileName + tableNum + ".xlsx";
        System.out.println(fieldName);
        // 创建文件
        File file = new File(fieldName);
        if (file.exists()) {
            file.delete();
        }
        if (!file.createNewFile()) {
            throw new IOException("无法创建文件: " + fieldName);
        }

        // 写入文件
        try (FileOutputStream fileOut = new FileOutputStream(file)) {
            // 这里假设有一个 workbook 对象
             workbook.write(fileOut);
        } catch (IOException e) {
            throw new IOException("写入Excel文件失败: " + e.getMessage(), e);
        }
    }

    /**
     * 写文件
     *
     * @param newStr 新内容
     * @throws IOException
     */

    //不覆盖写入
    public static void writeTxtFile(String newStr) throws IOException {
// 先读取原有文件内容，然后进行写入操作
        boolean flag = false;
        String temp = "";
        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        FileOutputStream fos = null;
        PrintWriter pw = null;

        try {
// 文件路径
            File file = new File(filenameTemp);
// 将文件读入输入流
            fis = new FileInputStream(file);
            isr = new InputStreamReader(fis);
            br = new BufferedReader(isr);
            StringBuffer buf = new StringBuffer();
// 保存该文件原有的内容
            for (int j = 1; (temp = br.readLine()) != null; j++) {
                buf = buf.append(temp);
// System.getProperty("line.separator")
// 行与行之间的分隔符 相当于“\n”
                buf = buf.append(System.getProperty("line.separator"));
            }

            buf.append(newStr);
            fos = new FileOutputStream(file);
            pw = new PrintWriter(fos);
            pw.write(buf.toString().toCharArray());
            pw.flush();
            flag = true;

        } catch (IOException e1) {
// TODO 自动生成 catch 块
            throw e1;
        } finally {
            if (pw != null) {
                pw.close();
            }

            if (fos != null) {
                fos.close();
            }

            if (br != null) {
                br.close();
            }

            if (isr != null) {
                isr.close();
            }

            if (fis != null) {
                fis.close();
            }

        }

    }

    //覆盖写入
    public static String LongNum(String filePath) {
        BigDecimal big = new BigDecimal(0);
        try {
            String encoding = "GBK";
            File file = new File(filePath);
            if (file.isFile() && file.exists()) { //判断文件是否存在
                InputStreamReader read = new InputStreamReader(new FileInputStream(file), encoding);//考虑到编码格式
                BufferedReader bufferedReader = new BufferedReader(read);
                String lineTxt = null;
                while ((lineTxt = bufferedReader.readLine()) != null) {
                    big = new BigDecimal(lineTxt).add(new BigDecimal(1));
                    PrintWriter pw = null;
                    FileOutputStream fos = null;
                    fos = new FileOutputStream(file);
                    pw = new PrintWriter(fos);
                    pw.write(big.toString());
                    pw.flush();
//                    System.out.println(big.toString());
                }

                read.close();
            } else {
                System.out.println("找不到指定的文件");
            }
        } catch (Exception e) {
            System.out.println("读取文件内容出错");
            e.printStackTrace();
        }
        return big.toString();
    }

    public static List<String> OnlyReadTxtFile(String filePath) throws Exception {
        String str = null;
        List<String> list = new ArrayList();
        try {
            String encoding = "UTF-8";
            File file = new File(filePath);
            if (file.isFile() && file.exists()) { //判断文件是否存在
                InputStreamReader read = new InputStreamReader(new FileInputStream(file), encoding);//考虑到编码格式
                BufferedReader bufferedReader = new BufferedReader(read);
                String lineTxt = null;
                while ((lineTxt = bufferedReader.readLine()) != null) {
                    list.add(lineTxt);
                }
                read.close();
            } else ObjException.method("找不到指定的文件");
        } catch (Exception e) {
            ObjException.method("读取文件内容出错:\n\t" + e);
        }
//        System.out.println(list);
        return list;
    }

//    /**
//     * 获取项目加载类的根路径
//     * @return
//     */
//    public static String localdemo.getPath(){
//        String path = "";
//        try{
//            //jar 中没有目录的概念
//            URL location = Application.class.getProtectionDomain().getCodeSource().getLocation();//获得当前的URL
//            File file = new File(location.localdemo.getPath());//构建指向当前URL的文件描述符
//            if(file.isDirectory()){//如果是目录,指向的是包所在路径，而不是文件所在路径
//                path = file.getAbsolutePath();//直接返回绝对路径
//            }else{//如果是文件,这个文件指定的是jar所在的路径(注意如果是作为依赖包，这个路径是jvm启动加载的jar文件名)
//                path = file.getParent();//返回jar所在的父路径
//            }
//            logger.info("project path={}",path);
//        }catch (Exception e){
//            e.printStackTrace();
//            logger.error("{}",e);
//        }
//        return path;
//    }

}



