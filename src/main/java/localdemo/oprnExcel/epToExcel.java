package localdemo.oprnExcel;

import com.alibaba.fastjson2.JSONObject;
import localdemo.dbConnection.gettingConnection;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.IOException;
import java.sql.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The type Db to excel.
 *
 * @author jjm
 */
public class epToExcel {
    /**
     * 设置日期格式
     */
    private static final DateTimeFormatter DF = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private static int CORE_POOL_SIZE = 5;

    static final int availableProcessors = Runtime.getRuntime().availableProcessors();

    // 内存中保存10000行，超过的实时写入磁盘，保证内存消耗不会过大
    private static final int rowAccessWindowSize = 10000;
    static int MAX_ROW = 1000000;

    private static String columns = "*";//查询字段
    private static String tableName = "new_psn_sz_d2024";//查询表名
    private static String DATA_SOURCE = "w_poc_basinfocent_db";//查询数据库
    static String useDbType;
    static String usePoc;
    static String url2;
    static String user2;
    static String password2;
    static String path;
    static String FILE_NAME;
    static int INDEX;

//    //useDbType:请确认使用的数据库(1:POC,2:oracel,3mysql)
////如果选择oracle或者自定义mysql库：useDbType选择2或者3并填好以下参数
//"url":"10.10.10.10:8080",
//        "user":"root",
//        "password2":"",
////DATA_SOURCE:查询的库名
////tableName:查询的表名
////columns:查询的字段名
////INDEX:导出表的个数
////MAX_ROW:单表的最大行数
////path:保存的文件路径
////FILE_NAME:保存的文件名
//
//    {
//        "useDbType":"1",
//            "url":"localhost:3306",
//            "user":"root",
//            "password":"",
//            "columns":"*",
//            "INDEX":5,
//            "MAX_ROW":10000,
//            "path":"D:\\resultData",
//            "FILE_NAME":"setlDtest",
//            "DATA_SOURCE":"hibiz_exp_db",
//            "tableName":"setl_d2409"
//    }

    public static void main(String[] args) throws Exception {
//        useDbType = "2";
//        usePoc = "Y";
//        columns = "*";
//        INDEX = 5;
//        MAX_ROW = 50000;
//        CORE_POOL_SIZE = 5;
//        path = "D:\\resultData";
//        FILE_NAME = "setlDtest";
//        DATA_SOURCE = "hibiz_exp_db";
//        tableName = "setl_d2409";
        System.out.println("请输入执行参数:");
        Scanner sc = new Scanner(System.in);
        StringBuilder input = new StringBuilder();
        while (true) {
            String line = sc.nextLine();
            if ("}".equalsIgnoreCase(line)) {
                break;
            }
            input.append(line);
        }
        input.append("}");
        JSONObject jsonObject ;
        jsonObject = JSONObject.parseObject(input.toString());
        // 获取每个字段的值
        url2 = jsonObject.getString("url");
        user2 = jsonObject.getString("user");
        password2 = jsonObject.getString("password");
        DATA_SOURCE = jsonObject.getString("DATA_SOURCE");
        useDbType = jsonObject.getString("useDbType");
        columns = jsonObject.getString("columns");
        INDEX = jsonObject.getIntValue("INDEX");
        MAX_ROW = jsonObject.getIntValue("MAX_ROW");
        path = jsonObject.getString("path");
        FILE_NAME = jsonObject.getString("FILE_NAME");
        tableName = jsonObject.getString("tableName");
        if("1".equals(useDbType)){
            if (!"hibiz_exp_db".equals(DATA_SOURCE)) {
                DATA_SOURCE = "w_" + DATA_SOURCE;
            }
        }
        jsonObject.forEach((k, v) -> System.out.println(k + ":" + v));
        exportData();
    }


    public static void exportData() {
        ExecutorService executor = Executors.newFixedThreadPool(availableProcessors);
        ExecutorService executorExcel = Executors.newFixedThreadPool(1);
        CompletableFuture<Void> futures1 = null;
        String beginTime = LocalDateTime.now().format(DF);
        System.out.println("程序开始时间:" + beginTime);
        try {
            //需要生成的表个数
            int tableNum = INDEX;
            //每个表最大行数
            int tableMaxRow = MAX_ROW;
            //线程数
            int theThreadCount = availableProcessors;
            //每次查询数
            int queryNum = tableMaxRow / theThreadCount;
            //最后一次查询数
            int lastQueryNum = tableMaxRow - queryNum * (theThreadCount - 1);
            int queryNumInc = 0;
//            System.out.println("queryNum:"+queryNum+",lastQueryNum:"+lastQueryNum);
            int begin = 0, end = 0;
            int theTableNum = 1;
            //循环查询数据库
            while (tableNum-- > 0) {
                String beginTime1 = LocalDateTime.now().format(DF);
                System.out.println("正在读取表[" + FILE_NAME + theTableNum + "]数据:" + beginTime1);
                //创建一个工作簿
                SXSSFWorkbook workbook = new SXSSFWorkbook(rowAccessWindowSize);
                //创建工作表sheet
                SXSSFSheet sheet = workbook.createSheet();
                //创建第一行作为表头
                SXSSFRow row = sheet.createRow(0);
                // 获取数据库连接
                Connection con = getConnection(useDbType, url2, user2, password2, DATA_SOURCE);
                //获取表头字段
                String sqlColumn = "select " + columns + " from " + tableName + " where false";
                PreparedStatement ps = con.prepareStatement(sqlColumn);
                ResultSetMetaData metaData = ps.getMetaData();
                int columnCount = metaData.getColumnCount();
                for (int i = 1; i <= columnCount; i++) {
                    // 写入表头字段
                    row.createCell(i - 1).setCellValue(metaData.getColumnName(i));
                }
                con.close();
                // 准备SQL查询语句
//                String str = "select " + columns + " from " + tableName + " where id > ? and id <= ?";
                Map<Integer, List<String>> resultMap = new ConcurrentHashMap<>();
                List<CompletableFuture<Void>> futures = new ArrayList<>();
                for (int i = 0; i < theThreadCount; i++) {
                    // 设置每次查询参数
                    begin = tableNum == INDEX - 1 ? i * queryNum : i * queryNum + queryNumInc;
                    end = i == theThreadCount - 1 ? begin + lastQueryNum : begin + queryNum;
                    String str = "select " + columns + " from " + tableName + " where id > " + begin + " and id <= " + end;
                    int finalI = i;
                    int finalBegin = begin;
                    int finalEnd = end;
                    futures.add(CompletableFuture.runAsync(() -> {
                        executeQuery(finalBegin, finalEnd, str, finalI, queryNum, resultMap);
                    }, executor));
                }
                // 等待所有任务完成
                CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).join();
                queryNumInc += tableMaxRow;
                System.out.println("表[" + FILE_NAME + theTableNum + "]数据读取完成，正在写入表:" + LocalDateTime.now().format(DF));
                int theTableNum1 = theTableNum++;
                futures1 = CompletableFuture.runAsync(() -> writeExcel(path, FILE_NAME, workbook, sheet, theTableNum1, resultMap,beginTime1),executorExcel);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            executor.shutdown();
            futures1.join();
            executorExcel.shutdown();
        }
        String endTime = LocalDateTime.now().format(DF);
        System.out.println("程序结束时间:" + endTime + "; 耗时:"
                + Duration.between(LocalDateTime.parse(beginTime, DF), LocalDateTime.parse(endTime, DF)).toMillis() / 1000 + "s");
    }

    private static void executeQuery(int finalBegin, int finalEnd, String str, int finalI, int queryNum, Map<Integer, List<String>> resultMap) {
        try (Connection con1 = getConnection(useDbType, url2, user2, password2, DATA_SOURCE);
             Statement ps1 = con1.createStatement()) {
            ResultSet rs = ps1.executeQuery(str);
            if (rs == null) {
                throw new SQLException("ResultSet is null:" + str + "; begin =" + finalBegin + "; end =" + finalEnd);
            }
            ResultSetMetaData data = rs.getMetaData();
            int indexNow = finalI * queryNum;
            while (rs.next()) {
                List<String> strings = new CopyOnWriteArrayList<>();
                for (int j = 1; j <= data.getColumnCount(); j++) {
                    strings.add(rs.getString(j));
                }
                resultMap.put(indexNow++, strings);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 根据条件从数据库获取数据并写入Excel文件
     * 此方法首先会根据传入的条件参数计算出数据库查询的起始和结束ID，
     * 然后执行数据库查询，如果查询到数据，则将这些数据写入到指定的Excel文件中
     */
    private static void writeExcel(String path, String FILE_NAME, SXSSFWorkbook workbook, SXSSFSheet sheet, int theTableNum, Map<Integer, List<String>> resultMap,String beginTime1) {
        if (resultMap.isEmpty()) {
            System.out.println("没有数据需要写入Excel文件");
            return;
        }
        int rowCount = 1;
        int size = resultMap.size();
        for (int i = 0; i < size; i++) {
            SXSSFRow currentRow = sheet.createRow(rowCount++);
            int columnIndex = 0; // 列索引
            for (String cellValue : resultMap.get(i)) {
                SXSSFCell cell = currentRow.createCell(columnIndex++);
                cell.setCellValue(cellValue);
            }
            //row的数量rowAccessWindowSize，从内存写入本地磁盘
            if (rowCount % rowAccessWindowSize == 0) {
                try {
                    sheet.flushRows();
                } catch (Exception e) {
                    throw new IllegalArgumentException("sheet从内存写入本地硬盘失败，行数: " + rowCount + ", 错误信息: " + e.getMessage(), e);
                }
            }
        }
        resultMap.clear();

        try {
            txtExport.writeExcelFile(path, FILE_NAME, theTableNum, workbook);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println("表[" + FILE_NAME + theTableNum + "]写入完成:" + LocalDateTime.now().format(DF)
                + ",耗时:" + Duration.between(LocalDateTime.parse(beginTime1, DF), LocalDateTime.now()).toMillis() / 1000 + "s");
    }


    /**
     * 获取数据库连接
     *
     * @return Connection 数据库连接对象
     * @throws Exception 如果获取连接时发生错误，抛出异常
     */
    public static Connection getConnection(String useDbType1, String url1, String user1, String password1, String dateSource) throws Exception {
        // 自定义MYSQL数据库连接
        if ("3".equals(useDbType1)) {
            return gettingConnection.gettingCustomCon(dateSource, url1, user1, password1);
        } else if ("2".equals(useDbType1)) {
            // 如果数据库类型为1，获取Oracle数据库连接
            return gettingConnection.getOracleConnection(dateSource, url1, user1, password1);
        } else {
            // 如果以上条件都不符合，连接POC数据库
            return gettingConnection.gettingCon(dateSource);
        }
    }

}