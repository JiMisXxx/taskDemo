package localdemo.apiRunDemo;

import localdemo.dbConnection.gettingConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class DbOperate {

    public static List<String> selectData(int i,int COREPOOLSIZE) throws Exception {
        String selectSql = "select psn_no from psninsuuploadtest where mod(id,"+COREPOOLSIZE+") = "+i+" and stats = '0' order by id limit 100;";
        Connection con = gettingConnection.gettingCon("test");
        PreparedStatement ps = con.prepareStatement(selectSql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        ResultSet rs = ps.executeQuery();
        if(rs==null){
            return null;
        }
        List<String> list = new ArrayList<>();
        while (rs.next()){
            list.add(rs.getString(1));
        }
        con.close();
        return list;
    }

    public static void upData(PsnOutInfo psnOutInfo, HashSet<String> hashSet) throws Exception {
        List<Map<String, String>> list = psnOutInfo.getData();
        String psnNo;
        String failRea;
        String insSql1 = "update psninsuuploadtest set MEMO = ?,STATS = ? where PSN_NO = ?";
        Connection con = gettingConnection.gettingCon("test");
//        con.setAutoCommit(false);
        PreparedStatement ps = con.prepareStatement(insSql1, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        if(list != null) {
            for (Map<String, String> map : list) {
                psnNo = map.get("psnNo");
                failRea = map.get("failRea");
                ps.setString(1, failRea);
                ps.setString(2, "2");
                ps.setString(3, psnNo);
                hashSet.remove(psnNo);
                ps.addBatch();
            }
            ps.executeBatch();
            ps.clearBatch();
        }
        StringBuilder sb = new StringBuilder();
        String insSql2 = "update psninsuuploadtest set MEMO = '成功',STATS = '1' where PSN_NO in (";
        sb.append(insSql2);
        for(String a:hashSet){
            sb.append("'").append(a).append("',");
        }
        sb.deleteCharAt(sb.length()-1).append(");");
        PreparedStatement ps2 = con.prepareStatement(sb.toString(), ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        ps2.executeUpdate();
//        con.commit();
        con.close();
    }
}
