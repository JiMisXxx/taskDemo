package localdemo.oprnExcel;

import localdemo.dbConnection.gettingConnection;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class dbToExcelOne implements Runnable {
    private static final DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");//设置日期格式
    static int threadcnt = 6; //启用线程数
    static AtomicInteger threadnow = new AtomicInteger(0);
    static int maxRow = 1000000;//单个Excel文件允许写入的最大行数
    List<String> myList = Arrays.asList("H44030300175","H44030300216","H44030300416","H44030300441","H44030300448","H44030300453","H44030300556","H44030300581","H44030300874","H44030400174","H44030400181","H44030400193","H44030400269","H44030400400","H44030402426","H44030500255","H44030500295","H44030500325","H44030500427","H44030501129","H44030501364","H44030501509","H44030501563","H44030502756","H44030600010","H44030600095","H44030600139","H44030600151","H44030600231","H44030600235","H44030600501","H44030600603","H44030600610","H44030600611","H44030600671","H44030600679","H44030600713","H44030600742","H44030601796","H44030602831","H44030700009","H44030700012","H44030700013","H44030700016","H44030700019","H44030700023","H44030700028","H44030700067","H44030700138","H44030700142","H44030700143","H44030700208","H44030700212","H44030700253","H44030700273","H44030700283","H44030700319","H44030700729","H44030700767","H44030700772","H44030701229","H44030701550","H44030702287","H44030703505","H44030900008","H44030900020","H44030900024","H44030900180","H44030900406","H44030900558","H44030900568","H44030901428","H44030901500","H44030903432","H44031100189");//每个Excel需要写入数据的查询条件
    static String str = "select * from cpmjz_info2022 WHERE \uFEFF机构编码 = ? ;";

    public static void main(String[] args) {
        running();
    }

    /**
     * 每个线程处理一个查询条件的数据写入到Excel
     */
    @Override
    public void run() {
        int threadnowint = threadnow.getAndIncrement();
        for (int j = threadnowint; j < myList.size(); j = j + threadcnt) {
            try {
                writeinto(myList.get(j),myList.get(j));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 多线程调用
     */
    public static void running() {
        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();;
        for (int i = 0; i < threadcnt; i++) {
            cachedThreadPool.execute(new dbToExcelOne());
        }
        cachedThreadPool.shutdown();
    }

    /**
     * POI SXSSFWorkbook写入Excel文件
     *
     * @param conditions
     */
    public void writeinto(String conditions, String tiaojian) throws Exception {
        System.out.println("文件[" + conditions + ".xlsx]正在获取数据，时间：" + LocalDateTime.now().format(df));
        Connection con = gettingConnection.gettingCon("w_poc_basinfocent_db");
        PreparedStatement ps = con.prepareStatement(str, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        ps.setString(1, tiaojian);
        ResultSet rs = ps.executeQuery();
        ResultSetMetaData data = rs.getMetaData();
        System.out.println("文件[" + conditions + ".xlsx]获取数据完成，开始写入数据，时间：" + LocalDateTime.now().format(df));
        ArrayList<String> celltitle = new ArrayList<>();
        //获取表头字段
        for (int i = 0; i < data.getColumnCount(); i++) {
            celltitle.add(data.getColumnName(i + 1));
        }
        int fnindex = 1;//用来记录需要创建的文件数
        while (true) {
            //创建Excel文件薄
            SXSSFWorkbook workbook = new SXSSFWorkbook();
            //创建工作表sheeet
            SXSSFSheet sheet = workbook.createSheet();
            //创建第一行,写入表头
            SXSSFRow row = sheet.createRow(0);
            for (int i = 0; i < celltitle.size(); i++) {
                SXSSFCell cell = row.createCell(i);
                cell.setCellValue(celltitle.get(i));
            }
            //写入数据
            int rows = 1, limit = 0;
            while (rs.next()) {
                SXSSFRow nextrow = sheet.createRow(rows++);
                for (int i = 1; i <= data.getColumnCount(); i++) {
                    SXSSFCell cell2 = nextrow.createCell(i - 1);
                    cell2.setCellValue(rs.getString(i));
                }
                //判断本次数据量是否达到写入Excel的最大行数
                if (++limit == maxRow) {
                    break;
                }
            }
            //limit等于0表示所有数据写入完毕，结束循环
            if (limit == 0) {
                break;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(conditions);
            //根据fnindex值判断是否要写入二个以上Excel文件
            if (fnindex != 1) {
                sb.append("-").append(fnindex);
            }
            //创建一个文件
            File wfile = new File("C:/Users/admin/Desktop/结果集/" + sb.toString() + ".xlsx");
            FileOutputStream op = new FileOutputStream(wfile);
            workbook.write(op);
            op.flush();
            op.close();
            System.out.println("文件[" + sb.toString() + ".xlsx]写入成功！完成时间：" + LocalDateTime.now().format(df));
            fnindex++;
        }
    }
}
