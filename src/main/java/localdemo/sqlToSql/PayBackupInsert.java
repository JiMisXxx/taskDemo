package localdemo.sqlToSql;

import localdemo.dbConnection.gettingConnection;
import localdemo.oprnExcel.txtExport;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Types;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PayBackupInsert {
    private static final DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");//设置日期格式
    static StringBuilder sbInsPocPolPay = new StringBuilder();
    static StringBuilder sbDelPocPolPay = new StringBuilder();
    static StringBuilder sbInsPrdPolPay = new StringBuilder();
    static StringBuilder sbInsPocPol = new StringBuilder();
    static StringBuilder sbInsPrdPol = new StringBuilder();
    static StringBuilder sbDel = new StringBuilder();
    static File directory = new File("");// 参数为空
    static String courseFile;

    static {
        try {
            courseFile = directory.getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws Exception {
        if ("poc_polcent_paymtd_db".equals(args[0])) {
            List<String> myList = Arrays.asList("pol_elem_b", "pol_elem_detl_a", "pol_elem_conv_b", "pol_elem_comp_d", "pol_elem_gena_d", "pol_bas_b", "pol_poolarea_b", "pol_biz_cfg_d", "pol_cfg_comp_d", "pol_cond_b", "pol_cond_comp_d", "pol_trt_b", "pol_info_b", "pol_sec_comp_d", "pol_sec_fund_d");
            for (String list : myList) {
                String sql = "select * from " + list + " where POOLAREA_NO like '4403%';";
                insert(sql, "poc_polcent_paymtd_db", sbInsPocPolPay);
            }
            txtExport.creatTxtFile(courseFile + "\\POC_政策中心-支付方式.sql");
            txtExport.writeTxtFile(sbInsPocPolPay.toString());
            txtExport.creatTxtFile(courseFile + "\\POC_政策中心-支付方式_删除.sql");
            txtExport.writeTxtFile(sbDelPocPolPay.toString());
        }
        if ("sz_polcent_paymtd_db".equals(args[0])) {
            List<String> myList1 = Arrays.asList("pol_elem_b", "pol_elem_detl_a", "pol_elem_conv_b", "pol_elem_comp_d", "pol_elem_gena_d", "pol_bas_b", "pol_poolarea_b", "pol_biz_cfg_d", "pol_cfg_comp_d", "pol_cond_b", "pol_cond_comp_d", "pol_trt_b", "pol_info_b", "pol_sec_comp_d", "pol_sec_fund_d");
            for (String list : myList1) {
                String sql = "select * from " + list + " where POOLAREA_NO like '4403%';";
                insert(sql, "sz_polcent_paymtd_db", sbInsPrdPolPay);
            }
            txtExport.creatTxtFile(courseFile + "\\PRD_SZ政策中心-支付方式.sql");
            txtExport.writeTxtFile(sbInsPrdPolPay.toString());
        }
        if ("w_poc_polcent_db".equals(args[0])) {
            List<String> myList1 = Arrays.asList("RULE_D", "RULE_EFFT_D", "RULE_FITR_D", "RULE_INPT_D", "RULE_OUPT_D", "RULE_RSLT_D", "RULE_SUBO_D", "DCSTAB_OUPT_KPI_DEFN_D", "DCSTAB_ACTI_GRP_D ", "DCSTAB_ACTI_DEFN_D ", "DCSTAB_TTL_D ", "DCSTAB_CELL_D", "DCSTAB_ROW_ACTI_D ", "DCSTAB_ROW_OUPT_D ", "DCSTAB_ROW_D", "DCSTAB_D");
            for (String list : myList1) {
                String sql = "select * from " + list + " where ADMDVS like '4403%';";
                if ("RULE_FITR_D".equals(list)) {
                    sql = "select * from RULE_FITR_D where fitr_id like '4403%';";
                }
                insert(sql, "w_poc_polcent_db", sbInsPocPol);
            }
            txtExport.creatTxtFile(courseFile + "\\POC_政策中心.sql");
            txtExport.writeTxtFile(sbInsPocPol.toString());
        }
        if ("polcent_db".equals(args[0])) {
            List<String> myList1 = Arrays.asList("RULE_D", "RULE_EFFT_D", "RULE_FITR_D", "RULE_INPT_D", "RULE_OUPT_D", "RULE_RSLT_D", "RULE_SUBO_D", "DCSTAB_OUPT_KPI_DEFN_D", "DCSTAB_ACTI_GRP_D ", "DCSTAB_ACTI_DEFN_D ", "DCSTAB_TTL_D ", "DCSTAB_CELL_D", "DCSTAB_ROW_ACTI_D ", "DCSTAB_ROW_OUPT_D ", "DCSTAB_ROW_D", "DCSTAB_D");
            for (String list : myList1) {
                String sql = "select * from " + list + " where ADMDVS like '4403%';";
                if ("RULE_FITR_D".equals(list)) {
                    sql = "select * from RULE_FITR_D where fitr_id like '4403%';";
                }
                insert(sql, "polcent_db", sbInsPrdPol);
            }
            txtExport.creatTxtFile(courseFile + "\\PRD_SZ政策中心.sql");
            txtExport.writeTxtFile(sbInsPrdPol.toString());
        }
    }

    public static void insert(String tableName, String dataBaseName, StringBuilder sbIns) throws Exception {
        String[] sql = tableName.split(" ");
        sbDelPocPolPay.append("delete ").append("from ").append(sql[3]).append(" where POOLAREA_NO like '4403%';\n");
        System.out.println(tableName);
        Connection con = gettingConnection.gettingCon(dataBaseName);
        PreparedStatement ps = con.prepareStatement(tableName, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        ResultSet rs = ps.executeQuery();
        ResultSetMetaData data = rs.getMetaData();
        StringBuilder sb;
        ArrayList<Integer> list = new ArrayList<>();
        while (rs.next()) {
            sb = new StringBuilder();
            sb.append("INSERT INTO ").append(sql[3]).append(" (");
            for (int i = 0; i < data.getColumnCount(); i++) {
                sb.append(data.getColumnName(i + 1)).append(",");
                list.add(data.getColumnType(i + 1));
            }
            sb.deleteCharAt(sb.length() - 1).append(") VALUES( ");

            for (int i = 1; i <= data.getColumnCount(); i++) {
                if (Types.TINYINT == data.getColumnType(i) || Types.DECIMAL == data.getColumnType(i) || Types.INTEGER == data.getColumnType(i)
                        || Types.BIGINT == data.getColumnType(i) || Types.FLOAT == data.getColumnType(i) || Types.DOUBLE == data.getColumnType(i)) {
                    sb.append(rs.getString(i)).append(", ");
                } else if (rs.getString(i) == null) {
                    sb.append("null").append(", ");
                } else if ("sz_polcent_paymtd_db".equals(dataBaseName) && data.getColumnName(i).matches("UPDT_TIME|OPT_TIME|CRTE_TIME")) {
                    sb.append("now()").append(", ");
                } else {
                    String a = rs.getString(i);
                    char b;
                    StringBuilder charSb = new StringBuilder();
                    for (int j = 0; j < a.length(); j++) {
                        b = a.charAt(j);
                        if (b == '\"') {
                            charSb.append("\\\"");
                        } else if (b == '\'') {
                            charSb.append("\\'");
                        }else{
                            charSb.append(b);
                        }
                    }
                    sb.append("'").append(charSb).append("', ");
                }
            }
            sb.deleteCharAt(sb.length() - 2).append(");\n");
            sbIns.append(sb);
        }
        rs.close();
        ps.close();
        con.close();
//        System.out.println(list);
    }

    public static void del(String tableName) {
        sbDel.append("delete ").append(tableName).append("where  where POOLAREA_NO like '4403%';\n");
    }
}
