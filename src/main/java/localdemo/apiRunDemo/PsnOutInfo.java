package localdemo.apiRunDemo;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author admin
 */
@Data
public class PsnOutInfo {
     private String code;
     private String type;
     private String message;
     private List<Map<String, String>> data;
}
