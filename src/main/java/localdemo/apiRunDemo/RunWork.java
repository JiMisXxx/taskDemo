package localdemo.apiRunDemo;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 测试定时器.
 *
 * @author linzp
 * @version 1.0.0
 * CreateDate 2020/9/18 17:05
 */
public class RunWork implements Runnable {

    /**
     * 这是任务执行体，打印当前的时间.
     */
    @Override
    public void run() {
        Logger logger = LoggerFactory.getLogger(RunWork.class);
        logger.info("任务执行了>>>>当前时间 {}", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss")));
        System.out.println("任务执行了>>>>当前时间 {"+ LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss"))+"}");
    }
}