package localdemo.apiRunDemo;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author linzp
 * @version 1.0.0
 * CreateDate 2020/9/18 17:04
 */
class TimerDemo {

    public static void main(String[] args) {

        //1.创建线程池  它可安排命令给定延迟后运行或定期执行
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(2);

        //2.执行任务 （initialDelay为延迟时间，此处延迟0s，立即执行，perio为周期，此处每5s执行一次，后面为时间单位）
        scheduledExecutorService.scheduleAtFixedRate(new RunWork(), 0, 5, TimeUnit.SECONDS);

    }
}