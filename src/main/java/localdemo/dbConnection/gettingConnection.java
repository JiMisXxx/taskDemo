package localdemo.dbConnection;

import java.sql.*;
import java.sql.DriverManager;
import java.util.Objects;

public class gettingConnection {
    static String url;
    static String user;
    static String password;
    static String classMysqlName = "com.mysql.cj.jdbc.Driver";

    public static void main(String[] args) throws Exception {
        getOracleConnection("ybsjqyz","10.94.58.2:1521","szsjqy","SINObest$hz2022");
    }

    public static Connection gettingCon(String dbsource) throws Exception {
        conInformation(dbsource);
        return DriverManager.getConnection(url, user, password);
    }

    public static Connection gettingCustomCon(String dbsource,String url2,String user2,String password2) throws Exception {
        url2 = "jdbc:mysql://"+url2+"/"+dbsource;
        return DriverManager.getConnection(url2, user2, password2);
    }


    public static void conInformation(String dbsource) throws ClassNotFoundException {
        StringBuilder sbuf = new StringBuilder();
        if ("poc_polcent_paymtd_db".equals(dbsource)) {
            dbsource = dbsource.substring(4);
            url = "jdbc:mysql://19.15.76.173:15020/";
            url = sbuf.append(url).append(dbsource).toString();
            user = "poc_plc_pms_rw_ops_hz";
            password = "s>J~5b>F";
        } else if ("sz_polcent_paymtd_db".equals(dbsource)) {
            url = "jdbc:mysql://19.15.76.173:15515/";
            url = sbuf.append(url).append(dbsource).toString();
            user = "prd_plc_pms_ro_app_sz_hz";
            password = "2,{~yzg4";
        }
        else if (dbsource.startsWith("w")) {
            dbsource = dbsource.substring(2);
            url = "jdbc:mysql://19.15.76.173:15009/";
            url = sbuf.append(url).append(dbsource).toString();
            user = "cep_ro_app";
            password = "8B{M<@vH";
        }else if (("hibiz_exp_db").equals(dbsource)) {
            url = "jdbc:mysql://19.15.76.173:15451/";
            url = sbuf.append(url).append(dbsource).toString();
            user = "cpc_sz_rw_app";
            password = "FiHGC2^6";
        } else {
            url = "jdbc:mysql://localhost:3306/";
            url = sbuf.append(url).append(dbsource).toString();
            user = "root";
            password = "";
        }
        Class.forName(classMysqlName);
//        System.out.println("连接数据库"+dbsource+"成功!");
    }

    public static Connection getOracleConnection(String service_name,String url3,String user3,String password3) throws ClassNotFoundException, SQLException {
        url3 = "jdbc:oracle:thin:@"+url3+"/"+service_name;
//        if (Objects.equals(service_name, "ybsjqyz")) {
//            url = "jdbc:oracle:thin:@10.94.58.2:1521/";
//            url = sbuf.append(url).append(service_name).toString();
//            user = "szsjqy";
//            password = "SINObest$hz2022";
//        }
        String className = "oracle.jdbc.OracleDriver";
        Class.forName(className);
        Connection conn = DriverManager.getConnection(url3, user3, password3);
        if (conn != null) {
            System.out.println("连接源库:"+service_name+"成功");
        } else {
            System.out.println("连接源库:"+service_name+"失败");
        }
        return conn;
    }
}
