package localdemo.sqlToSql;

import localdemo.dbConnection.gettingConnection;
import localdemo.oprnExcel.txtExport;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;

public class bakupInsert {

    static StringBuilder sbIns = new StringBuilder();

    public static void main(String[] args) throws Exception {
        File directory = new File("");
        String courseFile = directory.getCanonicalPath();
        String sql = "select * from fixmedins_serv_mgt_d where fixmedins_code like 'H4403%' limit 10;";
        insert(sql, "w_poc_polcent_db", sbIns);
        txtExport.creatTxtFile(courseFile + "\\test.sql");
        txtExport.writeTxtFile(sbIns.toString());
    }

    public static void insert(String tableName, String dataBaseName, StringBuilder sbIns) throws Exception {
        String[] sql = tableName.split(" ");
        System.out.println("sql:"+ Arrays.toString(sql) +"\ntableName:"+tableName);
        Connection con = gettingConnection.gettingCon(dataBaseName);
        PreparedStatement ps = con.prepareStatement(tableName, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        ResultSet rs = ps.executeQuery();
        ResultSetMetaData data = rs.getMetaData();
        StringBuilder sb;
        ArrayList<Integer> list = new ArrayList<>();
        while (rs.next()) {
            sb = new StringBuilder();
            sb.append("INSERT INTO ").append(sql[3]).append(" (");
            for (int i = 0; i < data.getColumnCount(); i++) {
                sb.append(data.getColumnName(i + 1)).append(",");
                list.add(data.getColumnType(i + 1));
            }
            sb.deleteCharAt(sb.length() - 1).append(") VALUES( ");

            for (int i = 1; i <= data.getColumnCount(); i++) {
                if ("INSUTYPE".equals(data.getColumnName(i))) {
                    sb.append("'390',");
                }else if ("MEMO".equals(data.getColumnName(i))) {
                    sb.append("'20230504-测试库新医保增加居民390服务',");
                }else if (Types.TINYINT == data.getColumnType(i) || Types.DECIMAL == data.getColumnType(i) || Types.INTEGER == data.getColumnType(i)
                        || Types.BIGINT == data.getColumnType(i) || Types.FLOAT == data.getColumnType(i) || Types.DOUBLE == data.getColumnType(i)) {
                    sb.append(rs.getString(i)).append(", ");
                } else if (rs.getString(i) == null) {
                    sb.append("null").append(", ");
                } else if (data.getColumnName(i).matches("UPDT_TIME|OPT_TIME|CRTE_TIME")) {
                    sb.append("now()").append(", ");
                }  else {
                    String a = rs.getString(i);
                    char b;
                    StringBuilder charSb = new StringBuilder();
                    for (int j = 0; j < a.length(); j++) {
                        b = a.charAt(j);
                        if (b == '\"') {
                            charSb.append("\\\"");
                        } else if (b == '\'') {
                            charSb.append("\\'");
                        }else{
                            charSb.append(b);
                        }
                    }
                    sb.append("'").append(charSb).append("', ");
                }
            }
            sb.deleteCharAt(sb.length() - 2).append(");\n");
            sbIns.append(sb);
        }
        sbIns.append("COMMIT;");
        rs.close();
        ps.close();
        con.close();
//        System.out.println(list);
    }
}
