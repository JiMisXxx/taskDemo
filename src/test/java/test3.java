

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class test3{
    public static void main(String[] args) {
        //1.创建一个无序的Set集合,并添加元素;
        Set<Integer> a = new HashSet();
        a.add(1);
        a.add(3);
        a.add(5);
        a.add(6);
        a.add(4);
        a.add(2);
        //2.创建一个TreeSet的排序器;
        TreeSet<Integer> set = new TreeSet<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer t1, Integer t2) {
                //t1.compareTo(t2)  是从小到大正序排序,同理t2 . t1 就是倒序排序;
                return t1.compareTo(t2);
            }
        });
        //3.将添加好数据的无序Set添加到TreeSet集合中去;
        set.addAll(a);
        //4.进行遍历
        System.out.println(set);
    }
}
