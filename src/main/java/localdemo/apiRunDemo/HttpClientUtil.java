package localdemo.apiRunDemo;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import localdemo.MyException.ObjException;
import localdemo.dbConnection.gettingConnection;
import org.apache.poi.xssf.streaming.SXSSFCell;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


/**
 * @author jjm
 * @date 2023/4/27 21:37
 **/
public class HttpClientUtil {

    public static void main(String[] args) throws Exception {
        String insSql1 = "select psn_no from psninsuuploadtest where id >= ? and id <= ?";
        Connection con = gettingConnection.gettingCon("test");
        con.setAutoCommit(false);
        PreparedStatement ps = con.prepareStatement(insSql1, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        long between;
        int times = 1;
        LocalDateTime kaishi = LocalDateTime.now();
        System.out.println(LocalDateTime.now());
        for(int i = 100001;i <= 101000;i=i+100){
            ps.setInt(1, i);
            ps.setInt(2, i+99);
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData data = rs.getMetaData();
            List<String> list = new ArrayList<>();
            while (rs.next()){
                list.add(rs.getString(1));
            }
            JSONArray json = JSONArray.from(list);
            LocalDateTime startTime = LocalDateTime.now();
            JSONObject output =  doPost("http://19.15.80.59/pic/api/psnInsuChg/psnInsuUpload",json);
            between = Duration.between(startTime, LocalDateTime.now()).getSeconds();
            System.out.println("第"+times+"次接口调用时长："+ between+"s");
            PsnOutInfo psnOutInfo = JSON.to(PsnOutInfo.class, output);
            HashSet<String> hashSet = new HashSet<>(list);
            startTime = LocalDateTime.now();
            intoData(psnOutInfo,hashSet);
            between = Duration.between(startTime, LocalDateTime.now()).getSeconds();
            System.out.println("第"+times+"次回写日志时长："+ between+"s");
            times++;
        }
        between = Duration.between(kaishi, LocalDateTime.now()).getSeconds();
        System.out.println("已处理"+times*100+"人,总耗时"+between+"s");
//        List<String> myList = Arrays.asList("44030000000632608139","2","3","4");
//        HashSet<String> hashSet = new HashSet<>(myList);
//        JSONArray list = JSONArray.from(myList);
//        JSONObject output =  doPost("http://19.15.80.59/pic/api/psnInsuChg/psnInsuUpload",list);
//        PsnOutInfo psnOutInfo = JSON.to(PsnOutInfo.class, output);
//        intoData(psnOutInfo,hashSet);
//        System.out.println(psnOutInfo);
    }

    public static void intoData(PsnOutInfo psnOutInfo, HashSet<String> hashSet) throws Exception {
        List<Map<String, String>> list = psnOutInfo.getData();
        String psnNo;
        String failRea;
        String insSql1 = "update psninsuuploadtest set MEMO = ?,STATS = ? where PSN_NO = ?";
        Connection con = gettingConnection.gettingCon("test");
        con.setAutoCommit(false);
        PreparedStatement ps = con.prepareStatement(insSql1, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        if(list != null) {
            for (Map<String, String> map : list) {
                psnNo = map.get("psnNo");
                failRea = map.get("failRea");
                ps.setString(1, failRea);
                ps.setString(2, "2");
                ps.setString(3, psnNo);
                hashSet.remove(psnNo);
                ps.addBatch();
            }
            ps.executeBatch();
            ps.clearBatch();
        }
        StringBuilder sb = new StringBuilder();
        String insSql2 = "update psninsuuploadtest set MEMO = '成功',STATS = '1' where PSN_NO in (";
        sb.append(insSql2);
        for(String a:hashSet){
            sb.append("'").append(a).append("',");
        }
        sb.deleteCharAt(sb.length()-1).append(");");
        PreparedStatement ps2 = con.prepareStatement(sb.toString(), ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        ps2.executeUpdate();
        con.commit();
        con.close();
    }

    /**
     * 以post方式调用对方接口方法
     */
    public static JSONObject doPost(String pathUrl, JSONArray param) {
        OutputStreamWriter out = null;
        BufferedReader br = null;
        StringBuilder result = new StringBuilder();
        JSONObject jieguo = new JSONObject();
        try {
            URL url = new URL(pathUrl);

            //打开和url之间的连接
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            //设定请求的方法为"POST"，默认是GET
            //post与get的不同之处在于post的参数不是放在URL字串里面，而是放在http请求的正文内。
            conn.setRequestMethod("POST");

            //设置30秒连接超时
            conn.setConnectTimeout(30000);
            //设置30秒读取超时
            conn.setReadTimeout(30000);

            // 设置是否向httpUrlConnection输出，因为这个是post请求，参数要放在http正文内，因此需要设为true, 默认情况下是false;
            conn.setDoOutput(true);
            // 设置是否从httpUrlConnection读入，默认情况下是true;
            conn.setDoInput(true);

            // Post请求不能使用缓存
            conn.setUseCaches(false);

            //设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");  //维持长链接
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");

            //连接，从上述url.openConnection()至此的配置必须要在connect之前完成，
            conn.connect();

            /**
             * 下面的三句代码，就是调用第三方http接口
             */
            //获取URLConnection对象对应的输出流
            //此处getOutputStream会隐含的进行connect(即：如同调用上面的connect()方法，所以在开发中不调用上述的connect()也可以)。
            out = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            //发送请求参数即数据
            out.write(param.toString());
            //flush输出流的缓冲
            out.flush();

            /**
             * 下面的代码相当于，获取调用第三方http接口后返回的结果
             */
            //获取URLConnection对象对应的输入流
            InputStream is = conn.getInputStream();
            //构造一个字符流缓存
            br = new BufferedReader(new InputStreamReader(is));
            String str = "";
            while ((str = br.readLine()) != null) {
                result.append(str);
            }
            jieguo = JSON.parseObject(String.valueOf(result));
            //关闭流
            is.close();
            //断开连接，disconnect是在底层tcp socket链接空闲时才切断，如果正在被其他线程使用就不切断。
            conn.disconnect();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return jieguo;
    }

    /**
     * 以get方式调用对方接口方法
     *
     */
    public static String doGet(String pathUrl) {
        BufferedReader br = null;
        String result = "";
        try {
            URL url = new URL(pathUrl);

            //打开和url之间的连接
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            //设定请求的方法为"GET"，默认是GET
            //post与get的不同之处在于post的参数不是放在URL字串里面，而是放在http请求的正文内。
            conn.setRequestMethod("GET");

            //设置30秒连接超时
            conn.setConnectTimeout(100000);
            //设置30秒读取超时
            conn.setReadTimeout(100000);

            // 设置是否向httpUrlConnection输出，因为这个是post请求，参数要放在http正文内，因此需要设为true, 默认情况下是false;
            conn.setDoOutput(true);
            // 设置是否从httpUrlConnection读入，默认情况下是true;
            conn.setDoInput(true);

            // Post请求不能使用缓存(get可以不使用)
            conn.setUseCaches(false);

            //设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");  //维持长链接
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");

            //连接，从上述url.openConnection()至此的配置必须要在connect之前完成，
            conn.connect();

            /**
             * 下面的代码相当于，获取调用第三方http接口后返回的结果
             */
            //获取URLConnection对象对应的输入流
            InputStream is = conn.getInputStream();
            //构造一个字符流缓存
            br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String str = "";
            while ((str = br.readLine()) != null) {
                result += str;
            }
            System.out.println(result);
            //关闭流
            is.close();
            //断开连接，disconnect是在底层tcp socket链接空闲时才切断，如果正在被其他线程使用就不切断。
            conn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
