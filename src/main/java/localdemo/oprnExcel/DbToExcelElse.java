package localdemo.oprnExcel;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import localdemo.MyException.ObjException;
import localdemo.dbConnection.gettingConnection;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * The type Db to excel.
 *
 * @author jjm
 */
public class DbToExcelElse implements Runnable {
    /**
     * 设置日期格式
     */
    private static final DateTimeFormatter DF = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    /**
     * 单个Excel文件允许写入的最大行数
     */
    static int MAX_ROW = 1000000;

    static String str = "select * from poc_psn_fix_b where id > ? and id <= ?";

    private static String DATA_SOURCE = "w_poc_basinfocent_db";

    static int CORE_POOL_SIZE = 5;
    static int MAXIMUM_POOL_SIZE = 20;
    static int INDEX = 10;
    static String FILE_NAME = "结算情况";
    static String path;
    static String usePoc = "Y";
    static int useDbType;
    static String url2;
    static String user2;
    static String password2;
    static int one;
    static int two;
    static String tbaleName;
    static String columns;

    public static void main(String[] args) {
        running();
    }


    /**
     * 多线程调用
     */
    public static void running() {
        /**
         * 参数信息：
         * int corePoolSize     核心线程大小
         * int maximumPoolSize  线程池最大容量大小
         * long keepAliveTime   线程空闲时，线程存活的时间
         * TimeUnit unit        时间单位
         * BlockingQueue<Runnable> workQueue  任务队列。一个阻塞队列
         */
        Scanner sc = new Scanner(System.in);
        Scanner sc2 = new Scanner(System.in);
//        String tbaleName = null;
        System.out.print("请确认数据库类型(1:oracle,2:mysql):");
        useDbType = sc.nextInt();
        sc.nextLine(); // 消费掉换行符
        if(useDbType != 1) {
            System.out.print("请确认是否要使用POC数据库Y/N(是/否):");
            usePoc = sc.nextLine().toUpperCase();
        }
        if (!"Y".equals(usePoc) || useDbType == 1) {
            System.out.print("请输入数据库地址(例:10.10.10.10:8080):");
            url2 = sc2.nextLine();
            System.out.print("请输入用户名:");
            user2 = sc2.nextLine();
            System.out.print("请输入密码:");
            password2 = sc2.nextLine();
            System.out.print("请输入要查询的库名:");
            DATA_SOURCE = sc2.nextLine();
            System.out.print("请输入查询的表名:");
            tbaleName = sc2.nextLine();
        }
        System.out.print("请输入要查询的字段名:");
        columns = sc2.nextLine();
        System.out.print("请输入导出表的个数:");
        INDEX = sc.nextInt();
        System.out.print("请输入每个表的最大行数:");
        MAX_ROW = sc.nextInt();
        System.out.print("请输入启动线程数:");
        CORE_POOL_SIZE = sc.nextInt();
        System.out.print("请输入文件保存路径:");
        path = sc2.nextLine();
        System.out.print("请输入文件名:");
        FILE_NAME = sc2.nextLine();
        if("Y".equals(usePoc) && useDbType != 1) {
            System.out.print("请输入查询的库名:");
            DATA_SOURCE = sc2.nextLine();
            if (!"hibiz_exp_db".equals(DATA_SOURCE)) {
                DATA_SOURCE = "w_" + DATA_SOURCE;
            }
            System.out.print("请输入查询的表名:");
            tbaleName = sc2.nextLine();
        }
//        url2 = "localhost:3306";
//        user2 = "root";
//        password2 = "";
//        DATA_SOURCE = "test";
//        tbaleName = "test1";
//        INDEX = 1;
//        MAX_ROW = 10;
//        CORE_POOL_SIZE = 1;
//        path = "C:\\Users\\admin\\Desktop";
//        FILE_NAME = "oracelTest";
//        useDbType = 11;
//        usePoc = "N";
//        str = "select * from " + tbaleName + " where id>" + one + " and id<=" + two;
        ThreadFactory namedThreadFactory = new ThreadFactoryBuilder()
                .setNameFormat("%d").build();
        ThreadPoolExecutor singleThreadPool = getThreadPoolExecutor(namedThreadFactory);
        //关闭线程池
        singleThreadPool.shutdown();
    }

    private static ThreadPoolExecutor getThreadPoolExecutor(ThreadFactory namedThreadFactory) {
        ThreadPoolExecutor singleThreadPool = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(4096), namedThreadFactory, new ThreadPoolExecutor.AbortPolicy());
        //执行任务
        for (int i = 0; i < INDEX; i++) {
            int finalI = i;
            singleThreadPool.execute(() -> {
                try {
                    writeinfo(finalI);
                } catch (Exception | ObjException e) {
                    e.printStackTrace();
                }
            });
        }
        return singleThreadPool;
    }

    /**
     * POI SXSSFWorkbook写入Excel文件
     *
     * @param conditions the conditions
     */
    public static void writeinfo(int conditions) throws Exception, ObjException {
        System.out.println("文件[" + FILE_NAME + conditions + ".xlsx]正在获取数据，时间：" + LocalDateTime.now().format(DF));
        ResultSet rs = null;
        Connection con;
        if (!"Y".equals(usePoc) && useDbType != 1) {
            con = gettingConnection.gettingCustomCon(DATA_SOURCE, url2, user2, password2);
        } else if (useDbType == 1) {
            con = gettingConnection.getOracleConnection(DATA_SOURCE, url2, user2, password2);
        } else {
            con = gettingConnection.gettingCon(DATA_SOURCE);
        }
        one = conditions * MAX_ROW;
        two = (conditions + 1) * MAX_ROW;
        str = "select "+columns+" from " + tbaleName + " where id>" + one + " and id<=" + two;
        try (PreparedStatement ps = con.prepareStatement(str, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)) {
            rs = ps.executeQuery();
            if (!rs.next()) {
                System.out.println("语句未查询到数据:" + str);
                return;
            }
            System.out.println("文件[" + FILE_NAME + conditions + ".xlsx]获取数据完成，开始写入数据，时间：" + LocalDateTime.now().format(DF));
            //将数据写入到Excel
            writeExcel(rs, conditions);
        }
    }

    private static void writeExcel(ResultSet rs, Object conditions) throws SQLException, IOException, ObjException {
        //用来记录需要创建的文件数
        int index = 1;
        ResultSetMetaData data = rs.getMetaData();
        // 内存中保存10000行，超过的实时写入磁盘，保证内存消耗不会过大
        int rowAccessWindowSize = 10000;
        while (true) {
            try (SXSSFWorkbook workbook = new SXSSFWorkbook(rowAccessWindowSize)) {
                //创建工作表sheet
                SXSSFSheet sheet = workbook.createSheet();
                //创建第一行,写入表头
                SXSSFRow row = sheet.createRow(0);
                //获取表头字段
                int columnCount = data.getColumnCount();
                for (int i = 0; i < columnCount; i++) {
                    SXSSFCell cell = row.createCell(i);
                    cell.setCellValue(data.getColumnName(i + 1));
                }
                //写入数据
                int rows = 1;
                int limit = 0;
                while (rs.next()) {
                    SXSSFRow nextrow = sheet.createRow(rows++);
                    for (int i = 1; i <= data.getColumnCount(); i++) {
                        SXSSFCell cell2 = nextrow.createCell(i - 1);
                        cell2.setCellValue(rs.getString(i));
                    }
                    //row的数量rowAccessWindowSize，从内存写入本地磁盘
                    if (rows % rowAccessWindowSize == 0) {
                        try {
                            sheet.flushRows();
                        } catch (Exception e) {
                            throw new ObjException("sheet从内存写入本地硬盘失败" + e.getMessage());
                        }
                    }
                    //判断本次数据量是否达到写入Excel的最大行数
                    if (++limit == MAX_ROW) {
                        break;
                    }
                }

                //limit等于0表示所有数据写入完毕，结束循环
                if (limit == 0) {
                    break;
                }
                StringBuilder sb = new StringBuilder();
                sb.append(FILE_NAME).append(conditions);
                //根据index值判断是否要写入二个以上Excel文件
                if (index != 1) {
                    sb.append("-").append(index);
                }
                //文件夹名
                if ('\\' != path.charAt(path.length() - 1)) {
                    path = path + "\\";
                }
                String fieldName = path + sb + ".xlsx";
                txtExport.creatTxtFile(fieldName);
                writeStream(fieldName, workbook);
                System.out.println("文件[" + sb + ".xlsx]写入成功！完成时间：" + LocalDateTime.now().format(DF));
                index++;
            }
        }
    }

    /**
     * 将SXSSFWorkbook对象写入到指定的文件中。
     *
     * @param fieldName 要写入的文件名
     * @param workbook  要写入的SXSSFWorkbook对象
     */
    private static void writeStream(String fieldName, SXSSFWorkbook workbook) {
        try (FileOutputStream stream = new FileOutputStream(fieldName)) {
            workbook.write(stream);
            stream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {

    }
}
