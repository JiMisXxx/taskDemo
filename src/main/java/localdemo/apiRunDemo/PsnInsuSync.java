package localdemo.apiRunDemo;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.HashSet;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class PsnInsuSync {
    private static final int COREPOOLSIZE = 1;
    static ThreadFactory namedThreadFactory = new ThreadFactoryBuilder()
            .setNameFormat("线程%d").build();
    static ThreadPoolExecutor singleThreadPool = new ThreadPoolExecutor(COREPOOLSIZE, COREPOOLSIZE,
            0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>(20), namedThreadFactory, new ThreadPoolExecutor.AbortPolicy());

    public static void main(String[] args) {
        runSync();
    }

    public static void runSync() {
        for(int i = 0; i < COREPOOLSIZE; i++){
            int finalI = i;
            singleThreadPool.execute(()->{
                try {
                    //查询未处理的人，一次100个人员编号
                    while (true) {
                        List<String> psnNo = DbOperate.selectData(finalI,COREPOOLSIZE);
                        if(psnNo == null){
                            break;
                        }
                        JSONArray json = JSONArray.from(psnNo);
                        //调用参保同步接口
                        JSONObject output = HttpClientUtil.doPost("http://19.15.80.59/pic/api/psnInsuChg/psnInsuUpload", json);
                        PsnOutInfo psnOutInfo = JSON.to(PsnOutInfo.class, output);
                        System.out.println("[线程:" + Thread.currentThread().getName() + "]:" + psnOutInfo);
                        HashSet<String> hashSet = new HashSet<>(psnNo);
                        //保存调用日志
                        DbOperate.upData(psnOutInfo, hashSet);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
